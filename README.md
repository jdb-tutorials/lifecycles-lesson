# lifecycles-lesson

Quick little project to go over [React's **lifecycles**](https://medium.com/stackavenue/react-lifecycle-methods-9faf9b434dbe). 

![React Lifecycle](https://i1.wp.com/programmingwithmosh.com/wp-content/uploads/2018/10/Screen-Shot-2018-10-31-at-1.44.28-PM.png?ssl=1)